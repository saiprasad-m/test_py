houses_in_street = input("Enter valid houses in street (eg., H-H--H-H-): ") 
rainsave_dict = { 'H' : [], '-' : []} 

def solution(houses_in_street):
    
    houses_in_street = houses_in_street.upper()
    print("Rainsaver street to setup : ", houses_in_street)
    count = 0
    for x in houses_in_street:
        if x in rainsave_dict:
            rainsave_dict[x] = rainsave_dict[x] + [count]  
        else:
            rainsave_dict[x] = [count]
        count = count + 1
        
    print(rainsave_dict)
    final = 0
    rainsave_keys = rainsave_dict.keys()
    house_lst = rainsave_dict[list(rainsave_keys)[0]]
    empty_lst = rainsave_dict[list(rainsave_keys)[1]]
    if len(house_lst) > 1:
        for i in range(0,len(house_lst),2): 
            if len(house_lst) % 2 == 0:
                if house_lst[i+1] - house_lst[i] == 2: 
                    final = final + 1
            else:
                try:
                    if house_lst[i+1] - house_lst[i] == 2: 
                        final = final + 1
                except IndexError:
                    final = final + 1
            # print("count", i, final)
    else:
        final = -1

    if final == 0: 
        final = -1
    if len(empty_lst) == 0:
        final = -1    
    print("No of rain savers needed: ", final)

solution(houses_in_street)