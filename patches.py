the_asphalt_patch_section = input("Enter valid asphalt pothole road section to fill (eg., XX...XX.): ") 
repair_dict = { 'X' : [], '.' : []} 

def solution(the_asphalt_patch_section):
    
    the_asphalt_patch_section = the_asphalt_patch_section.upper()
    print("Road to repair : ", the_asphalt_patch_section)

    count = 0
    for x in the_asphalt_patch_section:
        if x in repair_dict:
            repair_dict[x] = repair_dict[x] + [count]  
        else:
            repair_dict[x] = [count]
        count = count + 1
        
    print(repair_dict)
    patch_batch = 0
    set_watch = False
    final = 0
    lst_pothole = repair_dict[list(repair_dict.keys())[0]]
    lst_filled = repair_dict[list(repair_dict.keys())[1]]
    if len(lst_pothole) > 0:
        for i in range(0,lst_pothole[len(lst_pothole)-1]):

            if i in lst_pothole: 
                set_watch = True
                if i % 3 == 0 & set_watch:
                    patch_batch = patch_batch +1    
                    set_watch = False
                if set_watch:
                    final = final + 1
                
                if len(lst_pothole) + len(lst_filled) <= 5 | len(lst_filled) != 0:
                    final = final + 1 
                
                if final < len(lst_pothole):
                    final = len(lst_pothole) 
                if final > len(lst_filled):
                    final = patch_batch    
                if len(lst_filled) == 0:
                    final = i

            #print("count", i, patch_batch, set_watch, final)
    print("Minimum number of patches needed: ", final)

solution(the_asphalt_patch_section)