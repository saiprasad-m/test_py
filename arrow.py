
the_arrow_string = input("Enter valid arrow string to turn (eg., <vvv>>^): ") ## ">>>vv"
arrow_dict = {}    

def solution(the_arrow_string):
    the_arrow_string = the_arrow_string.replace(" ", "")
    the_arrow_string = the_arrow_string.lower()
    print("Given arrow string is: " + the_arrow_string)
    for x in the_arrow_string:
        if x in arrow_dict:
            arrow_dict[x] = arrow_dict[x] + 1
        else:
            arrow_dict[x] = 1

    sort_arrow_dict = dict(sorted(arrow_dict.items(), key=lambda item: item[1]))
    sort_keys = sort_arrow_dict.keys()
    sort_values = sort_arrow_dict.values()
    print(sort_arrow_dict)

    if len(sort_values) > 2:
        if int(list(sort_values)[0]) ==  int(list(sort_values)[len(sort_values) -1 ]) :  
            sort_arrow_dict.pop(""+str(list(sort_keys)[0]))
        else:
            sort_arrow_dict.pop(""+str(list(sort_keys)[len(sort_keys) -1 ]))
    else:
        sort_arrow_dict.pop(""+str(list(sort_keys)[len(sort_keys) -1 ]))
    print(sort_arrow_dict)
    print("Total turns required: ", sum(list(sort_arrow_dict.values())))

solution(the_arrow_string)